package com.oxigenao.pruebas;

import android.app.Activity;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.os.Bundle;
import android.os.Environment;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.util.Log;
import android.media.MediaRecorder;
import android.media.MediaPlayer;

import java.io.IOException;

import com.oxigenao.savemyriff.R;
import com.oxigenao.savemyriff.R.id;
import com.oxigenao.savemyriff.R.layout;

public class AudioRecord extends Activity {

	public MediaRecorder recorder;
	public String mFileName;

	public Button play;
	public Button rec;
	public Button stop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.record_layout);

		mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
		mFileName += "/audiorecordtest.3gp";

		recorder = new MediaRecorder();
		recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

		recorder.setOutputFile("mFileName");
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB);

		try {
			recorder.prepare();
		} catch (IllegalStateException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		play = (Button) findViewById(R.id.btn_play);
		rec = (Button) findViewById(R.id.btn_rec);
		stop = (Button) findViewById(R.id.btn_stop);

		Log.d("riff", "botonoes luistos");

		rec.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				recorder.start();

			}
		});

		stop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				recorder.stop();
				recorder.release();
				rec.setFocusable(false);
			}
		});

		play.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				MediaPlayer mPlayer = new MediaPlayer();
				try {
					mPlayer.setDataSource(mFileName);
					mPlayer.prepare();
					mPlayer.start();
				} catch (IOException e) {

				}

			}
		});

	}

}