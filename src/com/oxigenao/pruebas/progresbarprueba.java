package com.oxigenao.pruebas;

import com.oxigenao.savemyriff.R;
import com.oxigenao.savemyriff.R.id;
import com.oxigenao.savemyriff.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

public class progresbarprueba extends Activity {

	private static final int PROGRESS = 0x1;

	private ProgressBar mProgress;
	private int mProgressStatus = 0;
	public int c = 0;
	private Handler mHandler = new Handler();

	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		c=0;
		setContentView(R.layout.prueba);

		mProgress = (ProgressBar) findViewById(R.id.progressBar1);

		// Start lengthy operation in a background thread
		new Thread(new Runnable() {
			public void run() {
				while (mProgressStatus < 100) {
					mProgressStatus = doWork();

					// Update the progress bar
					mHandler.post(new Runnable() {
						public void run() {
							mProgress.setProgress(mProgressStatus);
						}
					});
				}
			}

			private int doWork() {

				if (c != 1000000) {
					c++;
				}

				return c/1000 ;
			}
		}).start();
	}

}
