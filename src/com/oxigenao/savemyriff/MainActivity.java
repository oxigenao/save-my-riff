package com.oxigenao.savemyriff;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.ls.LSInput;

import android.R.string;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	public ListView ListaRiff;
	public riffAdapter adapter;
	public ImageButton add;
	public BaseDatos LosRiffs;
	public ListView list;

	//FUCKYEAH
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		list = (ListView) findViewById(R.id.milista);
		list.setClickable(true);

		cargarRiffs();
		add = (ImageButton) findViewById(R.id.addnew);

		add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(MainActivity.this, riffEditor.class);
				startActivity(i);

			}
		});

	}

	private void cargarRiffs() {
		LosRiffs = new BaseDatos(getApplicationContext());
		List<riff> listariff = LosRiffs.seleccionarTodos();
		LosRiffs.Cerrar();
		adapter = new riffAdapter(this, listariff);
		list.setAdapter(adapter);

	}

	private void showToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onResume() {
		cargarRiffs();
		super.onResume();

	}

}