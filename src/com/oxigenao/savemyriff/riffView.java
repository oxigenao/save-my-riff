package com.oxigenao.savemyriff;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

public class riffView extends Activity {

	public TextView titulo;
	public TextView coment;
	public ProgressBar mProgress;
	public ImageButton start;
	public ImageButton borrar;
	public ImageButton compartir;
	public BaseDatos LosRiffs;
	public String nombre, comentario;

	private static final int PROGRESS = 0x1;

	private int mProgressStatus = 0;
	public int c = 0;
	private Handler mHandler = new Handler();
	public AlertDialog alertDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.riff_view_layout);

		Intent intent = getIntent();
		LosRiffs = new BaseDatos(getApplicationContext());
		titulo = (TextView) findViewById(R.id.titulo);
		coment = (TextView) findViewById(R.id.comentario);
		mProgress = (ProgressBar) findViewById(R.id.barraprogreso);
		start = (ImageButton) findViewById(R.id.play);
		borrar = (ImageButton) findViewById(R.id.btn_delete);

		nombre = intent.getStringExtra("titulo").toString();
		comentario = intent.getStringExtra("comentario").toString();

		titulo.setText(nombre);
		coment.setText(comentario);

		alertDialog = new AlertDialog.Builder(this).create();

		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startBarra();

			}

		});

		borrar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				alertDialog.setTitle("Borrar riff");
				alertDialog
						.setMessage("�Esta seguro de que desea borrar este riff?");
				alertDialog.setButton("Borrar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								LosRiffs.Borrar(nombre);
								LosRiffs.Cerrar();
								finish();// here you can add functions
							}
						});
				alertDialog.setButton2("cancelar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								alertDialog.dismiss();// here you can add
														// functions
							}
						});

				alertDialog.show();

			}
		});

	}

	private void startBarra() {
		c = 0;
		mProgressStatus = 0;

		// Start lengthy operation in a background thread
		new Thread(new Runnable() {
			public void run() {
				while (mProgressStatus < 100) {
					mProgressStatus = doWork();

					// Update the progress bar
					mHandler.post(new Runnable() {
						public void run() {
							mProgress.setProgress(mProgressStatus);
						}
					});
				}
				mProgress.setProgress(0);
			}

			private int doWork() {

				if (c != 1000000) {
					c++;
				}

				return c / 1000;
			}
		}).start();

	}

}
