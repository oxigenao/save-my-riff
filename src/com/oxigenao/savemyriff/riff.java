package com.oxigenao.savemyriff;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

public class riff {

	public String title=null;
	public String comentario=null;
	public String audio=null;

	public riff(String title, String comentario) {
		super();

		this.title = title;
		this.comentario = comentario;

	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String Comentario) {
		this.comentario = Comentario;
	}

	public void setAudio(String uri) {
		this.audio = uri;
	}

	public String getAudio() {
		return audio;
	}

}