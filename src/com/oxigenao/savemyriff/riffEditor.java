package com.oxigenao.savemyriff;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class riffEditor extends Activity {
	
	public Button tab;
	public ImageButton save;
	public BaseDatos nuevoRiff;
	public EditText nombre;
	public EditText comentario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editor_layout);
		
		
		nuevoRiff = new BaseDatos(getApplicationContext());
		nombre = (EditText) findViewById(R.id.editTextTitulo);
		comentario = (EditText) findViewById(R.id.editTextComentario);
		
		tab = (Button) findViewById(R.id.btn_edit_tab);
		
		tab.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent a = new Intent(riffEditor.this, riffTabEditor.class);
				startActivity(a);
					
			}
		});
		
		
		save = (ImageButton) findViewById(R.id.btn_guardar);
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String a = nombre.getText().toString();
				if (a.equals("")){
					a="Sin nombre";
				}
				String b=comentario.getText().toString();
				if(b.equals("")){
					b="Sin Comentario";
				}
				
				nuevoRiff.insertar(a,b);
				nuevoRiff.Cerrar();
				finish();
				
				
				
			}
		});
		
	}

}
