package com.oxigenao.savemyriff;

import java.util.List;

import android.R.string;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TwoLineListItem;

public class riffAdapter extends BaseAdapter implements OnClickListener {
	private Context context;
	
	private static final int PROGRESS = 0x1;


	private List<riff> listRiffs;
	
	

	public riffAdapter(Context context, List<riff> listRiffs) {
		this.context = context;
		this.listRiffs = listRiffs;

		Log.d("rff", "entra en adapter constructor");
	}

	public int getCount() {
		return listRiffs.size();
	}

	public Object getItem(int position) {
		return listRiffs.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup viewGroup) {

		Log.d("rff", "entra en getView");

		riff entry = listRiffs.get(position);
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.single_riff, null);
		}



		TextView title = (TextView) convertView
				.findViewById(R.id.single_riff_title);
		title.setText(entry.getTitle());

		TextView coment = (TextView) convertView
				.findViewById(R.id.single_riff_comentario);
		coment.setText(entry.getComentario());

		// Set the onClick Listener on this button
		ImageButton play = (ImageButton) convertView
				.findViewById(R.id.single_riff_play);

		play.setFocusableInTouchMode(false);
		play.setFocusable(false);
		play.setOnClickListener(this);
		// Set the entry, so that you can capture which item was clicked and
		// then remove it
		// As an alternative, you can use the id/position of the item to capture
		// the item
		// that was clicked.
		play.setTag(entry);

		// btnRemove.setId(position);

		return convertView;
	}

	@Override
	public void onClick(View view) {

		riff entry = (riff) view.getTag();
		Toast.makeText(context, entry.getTitle(), Toast.LENGTH_SHORT).show();
		notifyDataSetChanged();
		
		Intent myIntent = new Intent(view.getContext(), riffView.class);
		myIntent.putExtra("titulo", entry.getTitle());
		myIntent.putExtra("comentario", entry.getComentario());
        view.getContext().startActivity(myIntent);
		

	}


	private void showDialog(riff entry) {
		// Create and show your dialog
		// Depending on the Dialogs button clicks delete it or do nothing
	}

}