package com.oxigenao.savemyriff;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BaseDatos {

	private static final String DATABASE_NAME = "RIFF";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE_NAME = "MisRiffs";
	UsuariosSQLiteHelper openHelper;
	private Context context;
	private SQLiteDatabase db;

	public BaseDatos(Context context) {
		this.context = context;
		openHelper = new UsuariosSQLiteHelper(this.context);
		db = openHelper.getWritableDatabase();
		
		Log.d("riff", "creando base datos");
	}

	public boolean insertar(String nombreRiff, String comentarioRiff) {
		if (!db.isOpen()) {
			db = openHelper.getWritableDatabase();
		}

		ContentValues riffNuevo = new ContentValues();
		
		riffNuevo.put("nombre", nombreRiff);
		riffNuevo.put("comentario", comentarioRiff);
		Log.d("riff", "A�adiendo");

		return db.insert("MisRiffs", null, riffNuevo) != -1;
	}

	public boolean Borrar(String name) {
		if (!db.isOpen()) {
			db = openHelper.getWritableDatabase();
		}
		return 0 != db.delete("MisRiffs", "nombre" + "=" + "'" + name + "'",
				null);

	}

	

	public List<riff> seleccionarTodos() {

		
		
		
		if (!db.isOpen()) {
			db = openHelper.getWritableDatabase();
		}
		List<riff> lista = new ArrayList<riff>();

		Cursor cursor = this.db.rawQuery("SELECT * FROM MisRiffs", null);
		if (cursor.moveToFirst()) {
			do {
				Log.d("riff","cogiendo riffs");
				lista.add(new riff(cursor.getString(1), cursor.getString(2)));
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return lista;
	}

	// aqui tienes que devolver los riff

	/*public List<String> selectAll() {
		if (!db.isOpen()) {
			db = openHelper.getWritableDatabase();
		}
		List<String> list = new ArrayList<String>();
		// Cursor cursor = this.db.query(TABLE_NAME, new String[] { "adv" },
		// null, null, null, null, null);
		Cursor cursor = this.db.rawQuery("SELECT riff FROM MyRiffs", null);
		if (cursor.moveToFirst()) {
			do {
				list.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return list;
	}*/

	public boolean Existe(String name) {
		if (!db.isOpen()) {
			db = openHelper.getWritableDatabase();
		}

		Cursor c = null;
		try {
			c = db.rawQuery("SELECT * from `MisRiffs` WHERE `name` = " + name,
					null);
		} catch (SQLiteException e) {
			e.printStackTrace();
		}

		return c.moveToFirst();
	}

	public void Cerrar() {
		db.close();
	}

	private static class UsuariosSQLiteHelper extends SQLiteOpenHelper {

		public UsuariosSQLiteHelper(Context contexto) {
			super(contexto, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.d("riff", "SQLiteHelper onCreate");
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS `"
						+ TABLE_NAME
						+ "` (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nombre TEXT, comentario TEXT)");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int versionAnterior,
				int versionNueva) {

			Log.w("riff",
					"Upgrading database, this will drop tables and recreate.");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(db);

		}

	}

}